from parliament import Context
from numpy import matrix, linalg, random
from time import time
from datetime import datetime, timedelta
import os


def linpack(n):
    # LINPACK benchmarks
    ops = (2.0 * n) * n * n / 3.0 + (2.0 * n) * n

    # Create AxA array of random numbers -0.5 to 0.5
    A = random.random_sample((n, n)) - 0.5
    B = A.sum(axis=1)

    # Convert to matrices
    A = matrix(A)
    B = matrix(B.reshape((n, 1)))

    # Ax = B
    start = time()
    x = linalg.solve(A, B)
    latency = time() - start

    mflops = (ops * 1e-6 / latency)

    result = {
        'mflops': mflops,
        'latency': latency
    }

    return result


def main(context: Context):
    start = datetime.now() + timedelta(hours=7)
    n= int(context.request.data)
    result = linpack(n)
    
    node_name = os.environ.get('MY_NODE_NAME', None)
    pod_name = os.environ.get('MY_POD_NAME', None)
    #print(result)
    finish = datetime.now() + timedelta(hours=7)
    return {
        "result":result,
        "node": node_name,
        "pod": pod_name,
        "start": start.strftime("%m-%d-%Y %H:%M:%S"),
        "finish": finish.strftime("%m-%d-%Y %H:%M:%S")
        }