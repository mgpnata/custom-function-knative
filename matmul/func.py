from parliament import Context
import numpy as np
from time import time
from datetime import datetime, timedelta
import os


def matmul(n):
    A = np.random.rand(n, n)
    B = np.random.rand(n, n)

    start = time()
    C = np.matmul(A, B)
    latency = time() - start
    return latency
 
def main(context: Context):
    start = datetime.now() + timedelta(hours=7)
    n= int(context.request.data)
    result = matmul(n)
    
    node_name = os.environ.get('MY_NODE_NAME', None)
    pod_name = os.environ.get('MY_POD_NAME', None)
    #print(result)
    finish = datetime.now() + timedelta(hours=7)
    return {
        "time":result,
        "node": node_name,
        "pod": pod_name,
        "start": start.strftime("%m-%d-%Y %H:%M:%S"),
        "finish": finish.strftime("%m-%d-%Y %H:%M:%S")
        }