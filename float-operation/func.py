from parliament import Context
import math
from time import time
from datetime import datetime, timedelta
import os

def float_operations(n):
    start = time()
    for i in range(0, n):
        sin_i = math.sin(i)
        cos_i = math.cos(i)
        sqrt_i = math.sqrt(i)
    latency = time() - start
    return latency


def main(context: Context):
    start = datetime.now() + timedelta(hours=7)
    n= int(context.request.data)
    result = float_operations(n)
    
    node_name = os.environ.get('MY_NODE_NAME', None)
    pod_name = os.environ.get('MY_POD_NAME', None)
    #print(result)
    finish = datetime.now() + timedelta(hours=7)
    return {
        "time":result,
        "node": node_name,
        "pod": pod_name,
        "start": start.strftime("%m-%d-%Y %H:%M:%S"),
        "finish": finish.strftime("%m-%d-%Y %H:%M:%S")
        }
